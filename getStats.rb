
require 'gitlab'
require 'json'

GITLAB_HOSTNAME = 'https://gitlab.com'
PAT = '{YOUR-PAT-GOES-HERE}'
GROUP_ID = {ID-OF-YOUR-GROUP-GOES-HERE}

Gitlab.configure do |config|
  config.endpoint       = GITLAB_HOSTNAME + '/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT'] and falls back to ENV['CI_API_V4_URL']
  config.private_token  = PAT       # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
end

# retrieve all members of the specified group
members = Gitlab.group_members(GROUP_ID)

# for each member, call the "/users" API and export the desired data
members.each do |member|
	user = Gitlab.user(member.id)
	puts JSON.pretty_generate(user.to_h)
end

# example: show days inactive per user
today = Time.now.utc.to_date # in UTC as the API provides
members.each do |member|
	user = Gitlab.user(member.id)
	if(!user.last_activity_on.nil?)
		days_inactive = (today - Date.parse(user.last_activity_on)).to_i
		puts 'User ID:' + user.id.to_s + ' (username:' + user.username.to_s + ') ' + 'has been inactive for ' + days_inactive.to_s + ' days'
	else
		puts 'User ID:' + user.id.to_s + ' (username:' + user.username.to_s + ') ' + 'has no last_activity_on date'
	end
end
