# groupUserStats

***turns out, this only works if you're an admin on the instance since it depends upon data from the /users api endpoint that's `For Admins`***

Ruby script that uses https://github.com/NARKOZ/gitlab to poll GitLab's non-admin APIs for group member statistics (such as last-activity-on)

to run this script, update the constants as appropriate then simply do:

1. `gem install gitlab`
1. `ruby getStats.rb`
